/*
* Name: AlbumListItems
* Description: AlbumListItems Component
* Author: Gagudeep
* Date: 02 Apr, 2017
*/
import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const AlbumListItems = (props) => {
    return (
        <Card>
            <CardSection>
                <View style={styles.thumbnailContaierStyle}>
                    <Image style={styles.thubnailStyle} source={{ uri: props.albumData.thumbnail_image }} />
                </View>
                <View style={styles.headerContentStyle}> 
                    <Text style={styles.headerTextStyle}>{props.albumData.title}</Text>  
                    <Text>{props.albumData.artist}</Text>  
                </View>
            </CardSection>
            <CardSection>
                <Image style={styles.AlbumThubnailStyle} source={{ uri: props.albumData.image }} />
            </CardSection>
            <CardSection>
                <Button onPress={() => Linking.openURL(props.albumData.url)}>
                    Buy Now
                </Button>
            </CardSection>
        </Card>
    );
};

const styles = {
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    headerTextStyle: {
        fontSize: 18
    },
    thubnailStyle: {
        height: 50,
        width: 50
    },
    AlbumThubnailStyle: {
        height: 300,
        flex: 1,
        width: null
    },
    thumbnailContaierStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    }
}

export default AlbumListItems;