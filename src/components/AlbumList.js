/*
* Name: AlbumList
* Description: AlbumList Component
* Author: Gagudeep
* Date: 02 Apr, 2017
*/
import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';

import AlbumListItems from './AlbumListItems';

class AlbumList extends Component {
    state = {
        albums: []
    };

    /*
    * Description: React Life cycle method, It will load data when componentWillMount
    */
    componentWillMount() {
        // Axios is like ajax. It put the request to server and get data
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
            .then(response => this.setState({ albums: response.data }));
    }

    /*
    * Description: Get album list through map
    */
    renderAlbums() {
        return this.state.albums.map(album => 
            <AlbumListItems key={album.title} albumData={album}/>
        );
    }

    /*
    * Description: Render data to view
    */
    render() {
        console.log(this.state);
        return(
            <ScrollView>
                {this.renderAlbums()}
            </ScrollView>
        );
    }
}

export default AlbumList;