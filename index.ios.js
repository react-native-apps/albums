/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View
} from 'react-native';

import Header from './src/components/Header';
import AlbumList from './src/components/AlbumList';

export default class gpapp extends Component {
  render() {
    return(
      <View style={{ flex: 1 }}>
          <Header headerText={"Gugu Album"} />
          <AlbumList />
      </View>
      
    );
  }
}

AppRegistry.registerComponent("gpapp", () => gpapp);